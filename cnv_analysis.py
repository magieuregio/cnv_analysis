#!/usr/bin/python3

# -*- coding: utf-8 -*-


import glob
import subprocess
import sys
import os
import shutil
import pandas as pd
from sklearn_pandas import DataFrameMapper
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

if __name__ == "__main__":
    #GET BAM FILES
    main_folder=sys.argv[1]
    conv_folder=sys.argv[2]
    sample_files=glob.glob(main_folder+'sample/*.bam')
    
    #GET SAMPLES NAME
    print(sample_files)
    samples_id=[i.split('/')[-1] for i in sample_files]
    samples_id=[i.split('_')[0] for i in samples_id]
    print(samples_id)
    for sample in samples_id:
        #CREATE RESULT FOLDER
        print(os.path.exists(main_folder+'results/'+sample))
        if not os.path.exists(main_folder+'results/'+sample):
            os.makedirs(main_folder+'results/'+sample)
        else:
            print(f'folder already exist! skipping sample analysis for {sample}')
        shutil.move(f'{main_folder}sample/{sample}_final.bam', f'{main_folder}sample/inanalysis/')
        shutil.move(f'{main_folder}sample/{sample}_final.bai', f'{main_folder}sample/inanalysis/')

        #PERFORM PCA FOR CLUSTER SELECTION
        #CREATE NORMALIZED COUNTS FOR SAMPLE
        bashCommand='perl '+conv_folder+'CoNVaDING.pl -mode StartWithBam '+'-inputDir '+main_folder+'sample/inanalysis'+' -controlsDir '+main_folder+'controls/PCA/'+' -outputDir '+main_folder+'results/'+sample+' -bed '+main_folder+'bedfile/BED_OCULAR.bed'
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate() #DEBUG_ONLY
        files=glob.glob(main_folder+'controls/PCA/*.txt')
        DATA=pd.DataFrame()
        #READ NORMALIZED CONTROLS
        for file in files:
            t_=pd.read_csv(file, sep='\t')
            t_['sample']=file.split('/')[-1].split('.n')[0]
            t_['coordinates']=t_['CHR']+':'+t_['START'].map(str)+'-'+t_['STOP'].map(str)
            t_.set_index('coordinates',inplace=True)
            t_=t_[['NORMALIZED_TOTAL']]
            t_=t_.T
            t_.index = [file.split('/')[-1].split('.n')[0]]
            DATA=pd.concat([DATA,t_])
        #READ NORMALIZED SAMPLES
        path=main_folder+'results/'+sample+f'/{sample}_final.normalized.coverage.txt'
        t_=pd.read_csv(path, sep='\t')
        t_['sample']=path.split('/')[-1].split('.n')[0]
        t_['coordinates']=t_['CHR']+':'+t_['START'].map(str)+'-'+t_['STOP'].map(str)
        t_.set_index('coordinates',inplace=True)
        t_=t_[['NORMALIZED_TOTAL']]
        t_=t_.T
        t_.index = [path.split('/')[-1].split('.n')[0]]
        DATA=pd.concat([DATA,t_])
        #SCALING DATA
        mapper = DataFrameMapper([(DATA.columns, StandardScaler())])
        scaled_features = mapper.fit_transform(DATA.copy())
        scaled_features_df = pd.DataFrame(scaled_features, index=DATA.index, columns=DATA.columns)
        #PCA
        pca = PCA(n_components=2)
        principalComponents = pca.fit_transform(scaled_features_df)
        principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1', 'principal component 2'], index=scaled_features_df.index)
        cluster = AgglomerativeClustering(n_clusters=5, affinity='euclidean', linkage='ward')
        scaled_features_df['cluster']=cluster.fit_predict(principalDf)
        principalDf['cluster']=list(scaled_features_df['cluster'])
        clust_n=scaled_features_df.loc[f'{sample}_final','cluster']
        #GET CONTROLS CORRESPONDING TO SAMPLE CLUSTERS
        files.append(path)
        principalDf['paths']=files
        controls_to_load=list(principalDf[principalDf['cluster']==clust_n]['paths'])
        del controls_to_load[-1]
        #controls_to_load.to_csv(main_folder+'results/'+sample)
        for file in controls_to_load:
            shutil.move(file, main_folder+'controls/CONTROLS/')
        #NORMALIZE BAM COUNTS
        bashCommand='perl '+conv_folder+'CoNVaDING.pl -mode StartWithBam '+'-inputDir '+main_folder+'sample/inanalysis'+' -controlsDir '+main_folder+'controls/CONTROLS/'+' -outputDir '+main_folder+'results/'+sample+' -bed '+main_folder+'bedfile/BED_OCULAR.bed'
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate() #DEBUG_ONLY
        #CHOOSE CONTROLS
        bashCommand='perl '+conv_folder+'CoNVaDING.pl -mode StartWithMatchScore '+'-inputDir '+main_folder+'results/'+sample+' -controlsDir '+main_folder+'controls/CONTROLS/'+' -outputDir '+main_folder+'results/'+sample
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        #ANALYZE CNV
        bashCommand='perl '+conv_folder+'CoNVaDING.pl -mode StartWithBestScore '+'-inputDir '+main_folder+'results/'+sample+' -controlsDir '+main_folder+'controls/CONTROLS/'+' -outputDir '+main_folder+'results/'+sample
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        #INTEGRATE RESULTS WITH INFO FROM NORM COVERAGE AND BED FILE
        full_bed=pd.read_csv(sys.argv[3], sep='\t')
        coverage=pd.read_csv(main_folder+'results/'+sample+f'/{sample}_final.normalized.coverage.txt', sep='\t')
        calls=pd.read_csv(main_folder+'results/'+sample+f'/{sample}_final.best.score.shortlist.txt', sep='\t')
        calls_full={}
        j=0
        for index, row in calls.iterrows():
            strand=full_bed[full_bed['GENE']==row.GENE]['strand'].unique()
            if strand==1:
                temp=full_bed[full_bed['START']>=row.START]
                temp=temp[temp['END']<=row.STOP]
            else:
                temp=full_bed[full_bed['START']<=row.START]
                temp=temp[temp['END']>=row.STOP]
            for i,r in temp.iterrows():
                final_r=pd.concat([row.drop(['START','GENE']),r])
                calls_full[i]=final_r
                j=j+1
        result=pd.DataFrame(calls_full).T
        result['STOP']=result['END']
        result.drop(['END'],axis=1,inplace=True)
        result=result[['#CHROM', 'START', 'STOP', 'GENE','exone','strand', 'length', 'refseq', 'ABBERATION', 'NUMBER_OF_TARGETS',
               'NUMBER_OF_TARGETS_PASS_SHAPIRO-WILK_TEST']]
        result=result.reset_index(drop=True)
        result = pd.merge(result, coverage, on=['START','STOP', 'GENE'], how='left')
        result.to_csv(main_folder+'results/'+sample+f'/{sample}_final_indel.csv')
###############################################################################
        control_fin=pd.DataFrame()
        controls_files=glob.glob(f'{main_folder}controls/CONTROLS'+'/*.txt')
        for file in controls_files:
            temp=pd.read_csv(file,sep='\t')
            control_fin=pd.concat([control_fin,temp])
        l=[]
        ticks=[]
        genes=[]
        fig1, ax1 = plt.subplots(figsize=(15,10))
        colors=['g','b','c','y','silver','orange','salmon','khaki','chocolate']
        k=0
        j=0
        for i in range(0,len(sample)):
            l.append(control_fin[control_fin['START']==result.loc[i,'START']]['NORMALIZED_TOTAL'])
            ticks.append(result.loc[i,'GENE']+'_'+str(result.loc[i,'exone']))
            genes.append(result.loc[i,'GENE'])
        for i in range(0,len(result)):
            ax1.boxplot(l, whis=1.5)
            y = result.loc[i,'NORMALIZED_TOTAL']
            x = i+1
            if genes[i]==genes[k]:
                plt.axvspan(i+0.5, i+1.5, facecolor=colors[j], alpha=0.5)
            else:
                k=i
                if j < len(genes):
                    j=j+1
                else:
                    j=0
                plt.axvspan(i+0.5, i+1.5, facecolor=colors[j], alpha=0.5)
            plt.plot(x, y, 'r.', alpha=1, ms=10, marker='X')
        plt.xticks(range(1,len(result)+1), ticks)
        plt.savefig(main_folder+'results/'+sample+f'/{sample}_results.png')
###############################################################################
        files=glob.glob(main_folder+'controls/CONTROLS/*.txt')
        for file in files:
            shutil.move(file, main_folder+'controls/PCA/')
        shutil.move(f'{main_folder}sample/inanalysis/{sample}_final.bam', f'{main_folder}sample/')
        shutil.move(f'{main_folder}sample/inanalysis/{sample}_final.bai', f'{main_folder}sample/')
