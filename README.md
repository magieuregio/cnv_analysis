# Automate CNV analysis with CoNVaDING #

This pipeline can be executed in your Linux environment (you must install all dependencies). You must install pandas , sklearn_pandas, sklearn.cluster, matplotlib.

### How to run the pipeline in our own Linux environment? ##

Please execute the file `python3 cnv_analysis.py`.

## Input ##
In folder `sample` you must give the input .bam and .bai files, in `controls` the controls files, in `bedfile` the bed file used for the analysis.

## Output ##
In the `results` directory you will find a folder with the date in which you run the code and an Excel file with the results of the validation.
